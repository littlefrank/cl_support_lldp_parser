#CL-Support LLDP Parser

This script is designed to be run on a directory full of CL-support tarballs.

The script will extract all the tarballs using the OS's `tar` command and then analyze the LLDPctl output in the tarballs to build a [dot-file](https://docs.cumulusnetworks.com/cumulus-linux/Layer-1-and-Switch-Ports/Prescriptive-Topology-Manager-PTM/#basic-topology-example) graph.

This graph can be fed into [topology_converter](https://github.com/CumulusNetworks/topology_converter) to build a network simulation.


